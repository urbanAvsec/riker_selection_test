extern crate riker;
extern crate riker_default;
use std::thread;
use std::time::Duration;

use riker::actor::*;
use riker::system::ActorSystem;
use riker_default::DefaultModel;

fn main() {
    let model: DefaultModel<String> = DefaultModel::new();
    let sys = ActorSystem::new(&model).unwrap();
    sys.actor_of(TestActor::props(), "actor").unwrap();


    thread::sleep(Duration::from_millis(500))
}


struct TestActor;
impl Actor for TestActor {
    type Msg = String;

    fn post_start(&mut self, ctx: &Context<String>) {
        println!("Selection /user/foo");
        let selection : ActorSelection<String> = ctx.select("/user/foo/*").unwrap();
        selection.tell("Foo children".to_string(), None);
        println!("This doesn't execute")
    }

    fn receive(&mut self, ctx: &Context<<Self as Actor>::Msg>, msg: <Self as Actor>::Msg, sender: Option<ActorRef<<Self as Actor>::Msg>>) {
    }
}
impl TestActor {
    fn actor() -> BoxActor<String> {
        Box::new(TestActor)
    }
    fn props() -> BoxActorProd<String> {
        Props::new(Box::new(Self::actor))
    }
}
